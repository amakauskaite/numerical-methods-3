import java.util.Arrays;

// Atvirkstiniu iteraciju metodas
public class AIM {

    private double epsilon = 0.0001;
    private double pradinisArtinys = 0;
    private double lambda_artinys;
    private double lambda; //TR
    private double[] X;
    private double[] Y1;
    private double[] X1;
    private double[][] A;
    private int m; //iteraciju skaitiklis
    private int row;
    private int col;
    private double a; //intervalo pradzia
    private double b; //intervalo pabaiga
    private TriistrizainesSkaiciavimai ts = new TriistrizainesSkaiciavimai();

    public AIM(double[][] A) {

        System.out.println();
        System.out.println("-------------Atvirkstiniu iteraciju metodas--------------");
        System.out.println();
        this.A = A;
        row = A.length;
        col = A[0].length;
        m = 0;
        Y1 = new double[row];
        X = new double[row];
        X1 = new double[row];

        NustatytiIntervala();
        CharakteringojoDaugianarioRadimas();
        PradinioArtinioNustatymas();
        while (m<10) {
            NaujiArtiniai();
            if (skirtumasMazesnisUzEpsilon()) break;
            X = X1.clone();
            m++;
        }

        System.out.println();
        System.out.println("-------TV-----------");
        System.out.println("X : "+Arrays.toString(X));
        System.out.println("X1 : "+Arrays.toString(X1));
        System.out.println("Y1 : "+Arrays.toString(Y1));

        System.out.println("Iteraciju skaitiklis: "+m);


    }

    private boolean skirtumasMazesnisUzEpsilon() {
        double Xmax = Math.abs(X[0]);
        double X1max = Math.abs(X1[0]);

        for (int i = 0; i < row; i++) {
            if (Math.abs(X[i]) > Xmax)
                Xmax = X[i];
            if (Math.abs(X1[i]) > X1max)
                X1max = X1[i];
        }
        if (X1max - Xmax <= epsilon) return true;
        else return false;
    }

    //1 - Pradinio intervalo nustatymas
    private void NustatytiIntervala() {
        // Pagal Gersgorino teorema
        double[][] moduliuSuma = new double[row][2];

        for (int i = 0; i < row; i++) {
            moduliuSuma[i][0] = (-1) * A[i][i];
            for (int j = 0; j < col; j++) {
                if (i != j)
                    moduliuSuma[i][1] += Math.abs(A[i][j]);
            }
        }
        double[][] intervalai = new double[row][2];
        a = moduliuSuma[0][0] + moduliuSuma[0][1];
        b = moduliuSuma[0][1] - moduliuSuma[0][0];
        for (int i = 0; i < row; i++) {
            intervalai[i][0] = moduliuSuma[i][0] + moduliuSuma[i][1];
            intervalai[i][1] = moduliuSuma[i][1] - moduliuSuma[i][0];

            if (intervalai[i][0] < a)
                a = intervalai[i][0];

            if (intervalai[i][1] > b)
                b = intervalai[i][1];
        }

        System.out.println("Tikriniu reiksmiu intervalas: [" + a + ";" + b + ")");

    }

    // Charakteringojo daugianario radimas
    private void CharakteringojoDaugianarioRadimas() {
        // Rasime internavo galo a TR Sturmo grandine
        double[] p = new double[row + 1];
        int j = 0;

        lambda_artinys = a + b / 2;
        double[][] mat = matricaITriistrizaineMatrica(A);
        p[j] = 1;
        j += 1;
        p[j] = mat[0][1] - lambda_artinys;
        for (int i = 1; i < row; i++) {
            j += 1;
            p[j] = (mat[i][1] - lambda_artinys) * p[j - 1] - mat[i][0] * mat[i - 1][2] * p[j - 2];

        }

        System.out.println("----Charakteringojo daugianario reiksmes-----");
        System.out.println("Lambda: " + lambda_artinys);
        for (int i = 0; i <= row; i++) {
            System.out.println("p" + i + ": " + p[i]);
        }
        System.out.println("--------------------------------------------");
        System.out.println();

    }

    //3 - Tikriniu reiksmiu ir vektoriu apskaiciavimas
    //a - Pradinio artinio nustatymas
    private void PradinioArtinioNustatymas() {
        m = 0;
        lambda_artinys = 2.90393;
        for (int i = 0; i < row; i++) {
            X[i] = 1;
        }
        X[0]=0;
        X[row-1]=0;

        System.out.println("la : "+lambda_artinys);

    }

    //b - Naujas TV ir TR artinys
    private void NaujiArtiniai() {


        Y1 = ts.perkeltiesMetodas(matricaITriistrizaineMatrica(AminusLambdaI()), X, row);

        double max = Math.abs(Y1[0]);
        for (int i = 0; i < row; i++) {
            if (Math.abs(Y1[i]) > max)
                max = Y1[i];
        }

        for (int i = 0; i < row; i++) {
            X1[i] = Y1[i] / max;
        }

        //lambda_artinys = skaliarineSandauga(A*Y1, X1);
    }

    private double[][] AminusLambdaI() {
        double[][] temp = new double[row][col];
        temp = A.clone();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (i == j) {
                    temp[i][j] = (A[i][j] - lambda_artinys);
                } else temp[i][j] = A[i][j];
            }

        }
        return temp;
    }

    protected double[][] matricaITriistrizaineMatrica(double[][] neTI) {
        double[][] matrica = new double[row][3];
        int a = 0, b = 0, c = 1;

        for (int i = 0; i < row; i++) {
            if (i == 0)
            {
                matrica[i][0] = 0;
                matrica[i][1] = neTI[i][b];
                matrica[i][2] = neTI[i][c];
                b++;
                c++;
            }
            else if (i == (row - 1))
            {
                matrica[i][0] = neTI[i][a];
                matrica[i][1] = neTI[i][b];
                matrica[i][2] = 0;
                a++;
                b++;
                c++;
            }
            else {
                matrica[i][0] = neTI[i][a];
                matrica[i][1] = neTI[i][b];
                matrica[i][2] = neTI[i][c];
                a++;
                b++;
                c++;
            }
        }

        return matrica;
    }
}
