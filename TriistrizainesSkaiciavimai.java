import java.util.Arrays;

public class TriistrizainesSkaiciavimai {

    public double[] perkeltiesMetodas (double matrica[][], double d_matrica[], int matricos_dydis) {

        if (true) { //salygaPerkeltiesMetodui(matrica, matricos_dydis) == true
            double[] C = new double[matricos_dydis];
            double[] D = new double[matricos_dydis];

            C[0] = -1 * matrica[0][2]/matrica[0][1];
            D[0] = d_matrica[0] / matrica[0][1];
            //System.out.println("C1 = "+C[0]+", D1 = "+D[0]);

            //tiesiogine eiga
            for (int i = 1; i < matricos_dydis; i++) {
                double vardiklis = matrica[i][0] * C[i - 1] + matrica[i][1];
                if(i != matricos_dydis-1)
                C[i] = (-1) * matrica[i][2] / vardiklis;
                D[i] = (d_matrica[i] - (matrica[i][0] * D[i - 1])) / vardiklis;
                //if(i != matricos_dydis-1)
                //System.out.println("C"+(i+1)+" : " +C[i]);
                //System.out.println("D"+(i+1)+" : " +D[i]);

            }

            double[] x_matrica = new double[matricos_dydis];
            x_matrica[matricos_dydis - 1] = D[matricos_dydis - 1];
            //atbuline eiga
            for (int i = matricos_dydis - 2; i >= 0; i--) {
                x_matrica[i] = C[i] * x_matrica[i + 1] + D[i];
            }

            return x_matrica;

        }
        else return null;
    }

    public boolean salygaPerkeltiesMetodui (double matrica[][], int matricos_dydis)
    {
        int grieztaLygybe = 0;
        int negrieztaLygybe = 0;
        for (int i = 0; i < matricos_dydis; i++)
        {
            if(Math.abs(matrica[i][1]) >  Math.abs(matrica[i][0])+ Math.abs(matrica[i][2]))
            {
                ++grieztaLygybe;

            }
            if (Math.abs(matrica[i][1]) >=  Math.abs(matrica[i][0])+ Math.abs(matrica[i][2]))
            {
                ++negrieztaLygybe;
            }
        }

        System.out.println("negrieztu : " + negrieztaLygybe);
        System.out.println("grieztu : " + grieztaLygybe);
        System.out.println("matricos dydis : " + matricos_dydis);
        if(negrieztaLygybe == matricos_dydis && grieztaLygybe >=1)
            return  true;
        else return false;
    }
}
