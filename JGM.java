import java.util.Arrays;

// Jungtiniu gradientu metodas
public class JGM {

    private double epsilon = 0.0001;
    private double epsilon_kv = epsilon*epsilon;
    private double[] X;
    private double[] P;
    private double[] Z;
    private double[] Zp;
    private double[][] A;
    private double[] B;
    private double [] R;
    private double ZZ; //ZZ - Z ir Z skaliarine sandauga
    private double tau;
    private int row;
    private int col;
    private int k; //iteraciju skaitiklis
    private double ss;



    // Pradinio artinio parinkimas
    public JGM(double[][] matA, double[] matB, int iteracijuKiekis)
    {
        this.A = matA;
        this.B = matB;
        row = A.length;
        col = A[0].length;
        k = 0;
        X = new double[row];
        Zp = new double[row];
        for(int i=0; i<row; i++)
        {
            this.X[i] = 0;
        }

        Z = Z0();
        P = Z.clone();
        ZZ = SkaliarineSandauga(Z,Z);

        System.out.println();
        System.out.println("---------Jungtiniu gradientu metodas------------");
        System.out.println();
        //System.out.println();
        //System.out.println("Pradine skaliarine sandauga (Z,Z): " + ZZ);

        X = RastiArtini(iteracijuKiekis);
        System.out.println("---------Sprendinys----------");
        System.out.println("X : "+Arrays.toString(X));
        System.out.println("Iteracija : "+k);
        System.out.println("Paklaida : "+ss);
        System.out.println("Netiktis : "+RM.netiktis(X));
        System.out.println("------------------------------------------");
    }

    public double[] RastiArtini(int iteracijuKiekis)
    {
        while (k<iteracijuKiekis)
        {
            NaujoArtinioSkaiciavimas();
            if (BaigtiSkaiciavima()) break;
            NaujasJungtinisVektorius();
        }
        return X;
    }

    private double [] RSkaiciavimas()
    {
        R = new double[row];
        for(int i=0; i<row; i++)
        {
            for(int j=0; j<col; j++)
            {
                R[i] += A[i][j] * P[j];
            }
        }

        System.out.println("R : "+Arrays.toString(R));

        return R;
    }
    private double[] Z0()
    {
        double[] temp = new double[row];

        for(int i=0; i<row; i++)
        {
            for(int j=0; j<col; j++)
            {
                temp[i] += A[i][j] * X[j];
                //System.out.println(temp[i]);
            }
        }

        for (int i=0; i<row; i++)
        {
            temp[i] = temp[i] - B[i];
            System.out.println("Z0["+i+"] = " + temp[i]);
        }
        System.out.println();

        return temp;

    }


    private double SkaliarineSandauga(double[] Pirmas, double[] Antras)
    {
        double sandauga = 0;
        for(int i=0; i<row; i++)
        {
            sandauga += Pirmas[i] * Antras[i];
        }
        return sandauga;
    }


    //2 - Naujo Artinio Skaiciavimas
    private void NaujoArtinioSkaiciavimas()
    {
        System.out.println();
        System.out.println(k+" iteracija, naujo artinio skaiciavimas:");

        R = RSkaiciavimas();
        tau = SkaliarineSandauga(Z, P) / SkaliarineSandauga(R, P);
        System.out.println("tau : " + tau);

        ZXSkaiciavimas();
    }

    private void ZXSkaiciavimas()
    {
        Zp = Z.clone();

        for(int i=0; i<row; i++)
        {
            X[i] = X[i] - tau * P[i];
            Z[i] = Zp[i] - tau*R[i];
        }

        for (int i=0; i<row; i++)
        {
            System.out.println("Zk["+i+"] = "+Zp[i]+" ir Zk+1["+i+"] = "+Z[i]);
        }
        System.out.println("X : "+Arrays.toString(X));
    }
    //3 - Tikslumo tikrinimas
    private boolean BaigtiSkaiciavima()
    {
        ss = SkaliarineSandauga(Z,Z);
        System.out.println();
        System.out.println("Skaliarine sandauga = "+ss);
        System.out.println("Epsilon kvadratu = " + epsilon_kv);
        System.out.println();
        if (ss < epsilon_kv)
        {
            return true;
        }
        else return false;
    }

    //4 - naujo jungtinio vektoriaus skaiciavimas
    private  void NaujasJungtinisVektorius()
    {
        double beta = SkaliarineSandauga(Z, Z) / SkaliarineSandauga(Zp, Zp);
        System.out.println("Beta: "+beta);
        for(int i=0; i<row; i++)
        {
            System.out.println("P = " + P[i]);
            P[i] = Z[i] + beta*P[i];
            System.out.println("P["+i+"] : " + P[i]+"   Zp: "+Zp[i]+"  Z:"+Z[i]);
        }

        k++;
    }
}
