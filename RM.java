import javafx.css.CssParser;

import java.util.Arrays;

//Relaksacijos metodas
public class RM {

    private double omega = 1;
    private double paklaida = 0;
    private double epsilon = 0.0001;
    private double pradinisArtinys = 0;
    private double[] X;
    private double[] Xp;
    private double[][] A;
    private double[] B;
    private int k;
    private int row;
    private int col;
    private double delta;


    public RM(double[][] A, double[] B, int iteracijuKiekis) {
        this.A = A;
        this.B = B;
        row = A.length;
        col = A[0].length;
        k = 0;
        X = new double[row];
        Xp = new double[row];

        System.out.println();
        System.out.println("---------Relaksacijos metodas------------");
        System.out.println();

        RastiArtini(iteracijuKiekis);

        System.out.println("---------Sprendinys----------");
        System.out.println("X : " + Arrays.toString(X));
        System.out.println("Iteracija k : " + k);
        System.out.println("Paklaida : "+delta);
        System.out.println("Netiktis : "+netiktis());
        System.out.println("---------------------------------");
    }

    protected double netiktis()
    {
        double max = Math.abs(X[0]);
        for (int i = 0; i < row; i++) {
            if (Math.abs(X[i]) > max)
                max = X[i];
        }

        return max - 1.0025706940874035;
        //1.0025706940874035
    }

    protected static double netiktis(double[] X)
    {
        double max = Math.abs(X[0]);
        for (int i = 0; i < X.length; i++) {
            if (Math.abs(X[i]) > max)
                max = X[i];
        }

        return max - 1.0025706940874035;
        //1.0025706940874035
    }

    public void RastiArtini(int ik) {

        if (RelaksacijosParametrasTinkamas())
        {
            IstrizaineEiluteseVyraujamoji();
            IstrizaineStulpeliuoseVyraujamoji();
            PertvarkytiLygtis();
            PradinioArtinioParinkimas();
            while (k < ik) {
                NaujoArtinioApskaiciavimas();
                if (TikslumoTikrinimas())
                    break;
            }
        }
        else System.out.println("Bandykite su kitais duomenimis.");
    }

    public void PertvarkytiLygtis() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                //i-toji lygtis dalinama is koef. aii
                if (i != j)
                    A[i][j] = A[i][j] / A[i][i];
            }
            B[i] = B[i] / A[i][i];
        }

        System.out.println("A : " + Arrays.toString(A[0]) + Arrays.toString(A[1]) + Arrays.toString(A[2]));
        System.out.println("B : " + Arrays.toString(B));

    }

    public void PradinioArtinioParinkimas() {
        for (int i = 0; i < row; i++) {
            X[i] = pradinisArtinys;
        }

        System.out.println("Pradiniai x artiniai : " + Arrays.toString(X));
        epsilon = 0.0001;
        k = 0;
    }

    public void NaujoArtinioApskaiciavimas() {
        Xp = X.clone();
        X = B.clone();

        for (int i = 0; i < row; i++) {

            for (int j = 0; j <= i - 1; j++) {
                X[i] = X[i] - (A[i][j]*X[j]);
            }

            for (int j = i + 1; j < col; j++) {
                X[i] = X[i] - (A[i][j] * Xp[j]);
            }

            X[i] = Xp[i] + (omega*(X[i]-Xp[i]));
        }

        System.out.println(k + "-toji iteracija.");
        System.out.println("Xk : " + Arrays.toString(Xp));
        System.out.println("Xk+1 : " + Arrays.toString(X));
    }

    public boolean TikslumoTikrinimas() {
        delta = Math.abs(X[0] - Xp[0]);
        for (int i = 0; i < row; i++) {
            double xxp = Math.abs(X[i] - Xp[i]);
            if (xxp > delta)
                delta = xxp;
        }
        System.out.println("Xk+1 ir Xk max skirtumas : " + delta);
        if (delta <= epsilon)
            return true;
        else {
            k += 1;
            return false;
        }
    }

    public boolean RelaksacijosParametrasTinkamas()
    {
        if (omega < 2 && omega > 0)
            return true;
        else
        {
            System.out.println("Blogas relaksacijos parametras");
            return false;
        }
    }

    public boolean IstrizaineStulpeliuoseVyraujamoji()
    {
        //TODO: patikrinti ar gerai
        double sumaStulp;

        //vyrauja stulpeliuose
        for (int i = 0; i < col; i++)
        {
            sumaStulp = 0;
            for (int j = 0; j < row; j++)
            {
                if (i != j)
                    sumaStulp += A[j][i];
            }

            if (sumaStulp > A[i][i]) return false;
        }

        System.out.println("Istrizaine stulpeliuose vyraujamoji.");

        return true;
    }

    public boolean IstrizaineEiluteseVyraujamoji()
    {
        double sumaEil;

        //vyrauja eilutese
        for (int j = 0; j < row; j++)
        {
            sumaEil = 0;
            for (int i = 0; i < col; i++)
            {
                if (i != j)
                {
                    sumaEil += A[j][i];
                }
            }

            if (sumaEil > A[j][j]) return false;
        }

        System.out.println("Istrizaine eilutese vyraujamoji.");

        return true;
    }

}
