import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        //1 Uzdavinys:
        //0,0001 tikslumu isspresti TLS AX=B

         /*
           double[] B = new[] {1.941, -0.230, -1.941, 0.230};
           double[] C = new[] { 0.06, 0.06, 0.06, 0.06};
           double[][] D = new[][] { {1.342, 0.202, -0.599, 0.432},{0.202, 1.342, 0.202, -0.599},{-0.599, 0.202, 1.342, 0.202},{0.432, -0.599, 0.202, 1.342}};

           double[][] A = CalculateMatA(matC, matD);
           */


        //Jungtiniu gradientu metodo skaidriu pavyzdzio matricos


        double[] B = new double[] { 3.94, 4, 3.95};
        double[][] A = new double[][] {{2, 1, 0.95}, {1, 2, 1}, {0.95, 1, 2}};
        if (MatricaSimetrine(A) && MatricaTeigiamaiApibrezta(A))
        {
            JGM jgm = new JGM(A, B, 10);
        }



        //Zeidelio metodo pvz
        // Kai omega = 1, tai relaksacijos metodas sutampa su Zeidelio metodu
        //B = new double[]{11, 45, 14};
        //A = new double[][]{{10, -1, -3}, {2, 20, 3}, {2, 1, 10}};

        if (MatricaSimetrine(A) && MatricaTeigiamaiApibrezta(A))
        {
            RM rm = new RM(A, B, 7);
        }

        double[] C = new double[]{1.8, 1.8, 1.8, 1.8};
        double[][] T = new double[][]{{2.34, 2, 0, 0}, {2, 2.34, 2, 0}, {0, 2, 2.34, 2}, {0, 0, 2, 2.34}};
        double [][] matA = CalculateMatA(C, T);
        /*System.out.println(Arrays.toString(matA[0]));
        System.out.println(Arrays.toString(matA[1]));
        System.out.println(Arrays.toString(matA[2]));
        System.out.println(Arrays.toString(matA[3]));*/

        AIM aim = new AIM(matA);


    }

    public static double[][] CalculateMatA(double[] C, double[][] D) {

        double[][] A = new double[4][3];
        for (int i = 0; i < C.length; i++) {
            D[i][i] += C[i];
        }
        A = D.clone();
        return A;
    }

    public static boolean MatricaSimetrine(double[][] A)
    {
        int row = A.length;
        int col = A[0].length;

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                if(A[i][j] != A[j][i])
                {
                    System.out.println("---Matrica ne simetrine.---");
                    return false;
                }
            }
        }

        System.out.println("---Matrica simetrine.---");
        return true;
    }

    public static boolean MatricaTeigiamaiApibrezta(double[][]A)
    {
        System.out.println("---Tikiu, kad matrica teigiamai apibrezta---");
        return true;
    }
}
